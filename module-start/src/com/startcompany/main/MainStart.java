package  com.startcompany.main;

import com.apicompany.main.MainApi;
import com.dbcompany.main.MainDb;


public class MainStart {
    public static void main(String[] args) {

        MainApi mainApi = new MainApi();
        MainDb mainDb = new MainDb();

        mainApi.printTest();
        mainDb.printTest();
    }
}
